<?php
/**
 * Created by Haima.
 * Author:Haima
 * QQ:228654416
 * Date: 2018/9/12
 * Time: 5:39
 */

namespace HaimaPHP\core;

class Boot
{
    public static function run()
    {
        self::init();
        self::entry();
    }

    public static function entry()
    {
//        $entry = new \app\admin\controller\Entry;
//        $entry->index();

        $url_r = isset($_GET['r'])?strtolower($_GET['r']):'admin/entry/index';

        $arr_r = explode('/',$url_r); //index/entry/index
        //定义 模块/控制器/方法
        define('MODULE',$arr_r[0]);
        define('CONTROLLER',$arr_r[1]);
        define('ACTION',$arr_r[2]);

        $ClassName_path = '\app\\'. MODULE .'\controller\\' . ucfirst(CONTROLLER);
        $action = ACTION;
        $controller = new $ClassName_path;
        $controller->$action();
    }


    public static function init()
    {
        session_id() || session_start(); //如果没有session_id就开起session
        date_default_timezone_set('PRC');//设置时区
    }
}