<?php
/**
 * Created by Haima.
 * Author:Haima
 * QQ:228654416
 * Date: 2018/9/13
 * Time: 7:24
 */

namespace HaimaPHP\view;


class Base
{
    private $var = [];

    public function with($var)
    {
        $this->var = $var;
        return $this;
    }

    public function make()
    {
        extract($this->var);
        include '../app/' . MODULE . '/view/' . CONTROLLER . '/' . ACTION .'.php';
    }
}