<?php
/**
 * Created by Haima.
 * Author:Haima
 * QQ:228654416
 * Date: 2018/9/12
 * Time: 7:03
 */

namespace app\home\controller;



use HaimaPHP\view\View;

class Entry
{
    public function index()
    {
        //访问的是public/index,所以以它为起点目录
//        include  '../app/home/view/entry/index.php';
        //以当前目录为起点目录
//        include  __DIR__.'/../view/entry/index.php';
        $str='test';
        $arr=['a','b','c'];
        View::with(compact($str))->make();
    }
}