<?php
/**
 * Created by Haima.
 * Author:Haima
 * QQ:228654416
 * Date: 2018/9/12
 * Time: 7:03
 */

namespace app\home\controller;


use HaimaPHP\view\View;

class Number
{
    public function index()
    {
        echo 'home/Number/index';
        echo '<br>';
        echo 'module:'.MODULE;
        echo '<br>';
        echo 'controller:'.CONTROLLER;
        echo '<br>';
        echo 'action:'.ACTION;
        View::make();
    }

    public function test()
    {
        echo 'home/Number/test';
        echo '<br>';
        echo 'module:'.MODULE;
        echo '<br>';
        echo 'controller:'.CONTROLLER;
        echo '<br>';
        echo 'action:'.ACTION;
    }
}